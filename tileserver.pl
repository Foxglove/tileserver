#!/usr/bin/perl

#
# Serve a tile overlay for https://brouter.de/brouter-web/.
#
# The server prints it's the URL after startup:
#
#     sh# tileserver
#     INFO: Listening to requests of the form http://127.0.0.1:8181/{z}/{x}/{y}.png
#     ...
#

use strict;
use utf8;
use feature ':5.24';
use File::Basename qw/basename dirname/;
use Getopt::Long qw(:config gnu_getopt);
use File::Basename 'basename';
use Image::PNG::Libpng qw/ create_write_struct
                           write_png_file
                           write_to_scalar
                         /;
use Image::PNG::Const qw/ :all /;
use bytes;
use constant {
    #
    # Wording:
    #
    #  * "map tile": A tile of the map itself.  A map tile is always 256x256
    #        pixels.  The size is the same at all zoom levels.  The area shown
    #        is different: It grows by factor 2 for lower, and shrinks by factor
    #        2 for higher zoom levels.
    #  * "base tile": The ride-every-tile map tile. 256x256 pixels at zoom level
    #        14. A synonym for a certain area in the wild.
    #  * "overlay", or "overlay image": The transparent images we serve here as
    #        map overlay.  Depending on the zoom level, overlays have borders
    #        plus additional lines, that demarkate the borders of the base tiles
    #        in the current zoom level.
    #
    # We simply use an array-ref to store the tile "images":
    #
    #   * Zoom level 14 is the base zoom for ride every tile.  The overlay image
    #     (re-)used for this zoom is transparent with an eastern and southern
    #     border.
    #   * Zoom levels 0 through 10 reuse the same empty (i.e. transparent) image
    #     throughout.  Showing the borders of the base tiles in these zoom
    #     levels would be too much clutter.
    #   * Zoom levels 11 through 13 use just one zoom specific image wich is
    #     divided by lines.  As for example a 256x256 pixel map tile in zoom
    #     level 13 shows 4 base tiles, the overlay cuts this map tile in 4
    #     sections to show the base tiles.
    #   * Zoom levels 15 and above use the empty image, one with an eastern, one
    #     with a southern border, and the tile of zoom level 14 (the lower lower
    #     right corner so to speak).  Which of those images is returned depends
    #     on position of the requested map tile on the globe.
    #
    TILE_EMPTY => 0,               # ... reused from $images[1] to $images[10]
    TILE_SE => 14,                 # Base tile. Has eastern and southern border.
    TILE_E => 15,                  # We may use the indeces above 15 as we like
    TILE_S => 16,

    # Our "images" are array-refs themselves, each of which contains of it's
    # size in bytes and the data. The size is stored for use in the response
    # header.
    IDX_SIZE => 0,
    IDX_DATA => 1,
};
use base 'Net::Server::HTTP';



### @section Options

my $default_server_port = 8181;
my $opt_help;
my $opt_ipv4_only;                      # IPv6 might be slow on elder systems.
my $opt_server_port = $default_server_port;
my $opt_min_zoom = 11;

GetOptions(
    "help|h|?"      => \$opt_help,
    "ipv4-only|4"   => \$opt_ipv4_only,
    "port|p=i"      => \$opt_server_port,
    "min-zoom|mz|m=i" => \$opt_min_zoom,
)
    or die("There was a problem parsing the options.");

if( $opt_help ) {
    help();
    exit 0;
}



### @section Init Phase
#
# The overlay images are created on startup and kept in our array ref (see
# below).  That is, all functions concerned with image creation are called just
# during the init phase.  The datetime string for the 'Last-Modified' header is
# created during this phase, too, and re-used throughou the live span of the
# process.
#


my $images = [];           # Our overlay images. [ [ IDX_SIZE, IDX_DATA ], ... ]
load_images( $images );    # Create overlays.

# Generate the 'Last-Modified' header's value:
my $startup_datetime_header_value = make_last_modified_header_value();



### @section Startup Phase
#
# All done.  Let's start serving those overlays.
#

print STDERR "INFO: Listening to requests of the form http://127.0.0.1:$opt_server_port/{z}/{x}/{y}.png\n";

__PACKAGE__->run(
    port => $opt_server_port,
    ipv => ($opt_ipv4_only ? 'IPv4' : '*' ),
);



### @section Serve Overlays

#** @function process_http_request ()
#
# Overrides Net::Server::HTTP::process_http_request.
#*
sub process_http_request {
    my $self = shift;
    local( $1, $2, $3 );

    if( $ENV{'PATH_INFO'} =~ m{/([0-9]+)/([-0-9]+)/([-0-9]+)} )
    {
        my $z = $1;
        my $x = $2;
        my $y = $3;

        my $img = data_for_zxy($z, $x, $y);

        if( $img->[IDX_SIZE] ) {
            print "Content-Type: image/png\n",
                "Content-Length: $img->[IDX_SIZE]\n",
                "Last-Modified: $startup_datetime_header_value\n",
                "\n";
            print $img->[IDX_DATA];
            return;
        }
    }
    $self->send_status(404, 'File Not Found');
}


#** @method data_for_zxy ( $z, $x, $y )
#
# @return An "image".  Might be the entirely transparent one.
#*
sub data_for_zxy {
    my( $z, $x, $y ) = @_;

    if ( $z < 15 )                      # Just one tile reused throughout
    {
        return $images->[$z];
    }

    else                                # Zoom level 15 and above
    {
        # my $mod = 0;
        # if ( 16 == $z ) { $mod = 3; }     # z == 16: Every 4th tile gets a border
        # elsif ( 17 == $z ) { $mod = 7; }  # every 8th tile...
        # elsif ( 18 == $z ) { $mod = 15; } # every 16th tile...
        # ...
        #
        # Which we can reduce to:
        # 15 = 1; # ( 2 << 0 ) - 1
        # 16 = 3; # ( 2 << 1 ) - 1
        # 17 = 7; # ( 2 << 2 ) - 1
        # ...
        #
        # Which is in fact:
        my $mod = (2 << ($z - 15)) - 1;

        # Just in case... if $mod is less than zero, this branch is not appropriate.
        $mod >= 0 and return $images->[ idx_for_xy_mod( $x, $y, $mod) ];
    }

    return $images->[ TILE_EMPTY ];
}


#**
# @function idx_for_xy_mod ( $x, $y, $mod )
#
# Returns the key for image in %$images as appropriate to the paramters.
#
# Thanks to the binary nature ot the zoom algorithm (doubling width and height
# with every step), a modulo operation mutates, or rather is restricted to, a
# binary AND operation.
#
# @param x   X-offset of the tile we are searching the image for.
# @param y   Y-offset of the tile we are searching the image for.
# @param mod The number we need for our <code>$x & $mod</code>
#            resp. our <code>$y & $mod</code> operation.
#*
sub idx_for_xy_mod {
    my($x, $y, $mod ) = @_;
    if ( $mod == ($x & $mod) ) {        # east border?
        if ( $mod == ($y & $mod) ) {    # southern border?
            return TILE_SE;
        }
        else {
            return TILE_E;
        }
    }
    else {                              # no east border
        if ( $mod == ($y & $mod) ) {    # southern border?
            return TILE_S;
        }
    }
    return TILE_EMPTY;                  # no borders at all
}



### @section Loading of Images


sub load_images {
    my $images = pop();
    my $dir = dirname( __FILE__ );
    say STDERR "(Re-) loading images...";
    $images->[TILE_EMPTY] = make_image();

    # Empty tiles:
    for my $zoom ( 1 .. 10 ) {
        $images->[$zoom] = $images->[TILE_EMPTY];
    }

    # "Special" tiles are divieded in more than one field (lines...):
    for my $zoom ( $opt_min_zoom .. TILE_SE ) {
        my $nlines = 2 << (13 - $zoom);
        $images->[$zoom] = make_image( $nlines, $nlines );
    }

    $images->[TILE_E]     = make_image( 1, 0 );
    $images->[TILE_S]     = make_image( 0, 1 );
}


#** @function make_image ( $n_vlines, $n_hlines )
#
# Crate a 256x256 transparent PNG image with the number of horizontal and
# vertical lines given. The lines will always be drawn at the last pixel
# inside the division.
#
# Examples:
#   - make_image(0, 1): 1 horizontal line at the bottom of the image.
#   - make_image(1, 0): 1 vertical line at the right edge of the image.
#   - make_image(2, 2): Adds
#         - a horizontal line at v-pixel 127 (i.e. the 128th v-pixel),
#         - a horizontal line at the bottom of the image (v-pixel 255),
#         - a vertical line at h-pixel 127 (i.e. the 128th), and
#         - a vertical line at the right edge of the image (h-pixel 255,
#           i.e. the 256th).
#
# @param n_vlines Number of vertical lines.
# @param n_hlines Number of horizontal lines.
#*
sub make_image {
    my($vlines, $hlines) = @_;
    my $png = create_write_struct ();
    my $img_width = ($vlines || $hlines) ? 256 : 1;

    $png->set_IHDR({
        height => $img_width, width => $img_width, bit_depth => 8,
        color_type => PNG_COLOR_TYPE_GRAY_ALPHA
    });

    # Here 9 is max compression, 1 is max speed, 0 is no compression at all.
    $png->set_compression_level(9);

    my $rows = make_rows($vlines, $hlines, $img_width);
    $png->set_rows($rows);
    my $img = $png->write_to_scalar();
    return [ length $img, $img ];
}


#** @function make_rows ( $n_vlines, $n_hlines, $img_width )
#
# Create the actual image data and return it as an array reference.
#
# @param n_vlines Number of vertical lines (see make_image above).
# @param n_hlines Number of horizontal lines (see make_image above).
# @param img_width Width and height in pixels.
# @return Array ref image data.
#*
sub make_rows {

    use bytes;

    my( $vlines, $hlines, $img_width ) = @_;

    # @rows is an array reference with a number of rows equal to the height of
    # the PNG image. Each element of the array reference is a string containing
    # the binary data making up a row of the image.
    my @rows;

    # A pixel's first byte denotes the color (from \x00=black to, \xFF=white),
    # second byte transparency (from \x00=transparent to \xFF=opaque):
    #
    # substr( $rows[$y], $x*2, 2, "\xFF\x00" ); # 1111 0000:  white, 100% transparent
    # substr( $rows[$y], $x*2, 2, "\x00\x00" ); # 0000 0000:  black, 100% transparent
    # substr( $rows[$y], $x*2, 2, "\xFF\xFF" ); # 1111 1111:  white, 100% opaque
    # substr( $rows[$y], $x*2, 2, "\x00\xFF" ); # 0000 1111:  black, 100% opaque

    # Fill the tile with transparency first:
    for my $y ( 0 .. ($img_width - 1) )
    {
        $rows[$y] = "\xFF\x00" x $img_width;
    }

    # draw the lines:
    if( $hlines ) {
        for( my $i = ($img_width - 1); $i > 0; $i -= ($img_width / $hlines) ) {
            draw_horizontal_line ( \@rows, $i, $img_width );
        }
    }
    if( $vlines ) {
        for( my $i = ($img_width - 1); $i > 0; $i -= ($img_width / $vlines) ) {
            draw_vertical_line ( \@rows, $i, $img_width );
        }
    }

    return \@rows;
}


sub draw_vertical_line {

    use bytes;
    my( $rows, $x, $img_width ) = @_;

    for my $y ( 0 .. ($img_width - 1) )
    {
        substr( $rows->[$y], $x*2, 2, "\x00\xFF");
    }
}


sub draw_horizontal_line {

    use bytes;
    my( $rows, $y, $img_width ) = @_;

    for my $x ( 0 .. ($img_width - 1) )
    {
        substr( $rows->[$y], $x*2, 2, "\x00\xFF");
    }
}


sub make_last_modified_header_value {
    my($second, $minute, $hour, $day, $month, $year, $weekday, $yearday) = gmtime();

    my $dayname   = ( qw/ Sun Mon Tue Wen Thu Fri Sat / )[$weekday];
    my $monthname = ( qw/ Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec / )[$month];


    $month += 1;                        # month 0 == Januar; wday 0 = Sonntag
    $year  += 1900;
    ($second, $minute, $hour, $day, $month) = (
        sprintf("%02d", $second),
        sprintf("%02d", $minute),
        sprintf("%02d", $hour),
        sprintf("%02d", $day),
        sprintf("%02d", $month + 1),
    );

    # Tue, 02 May 2023 19:00:00 GMT
    return "$dayname, $day $monthname $year $hour:$minute:$second GMT";
}


sub help {
    my $prog = basename $0;

    say <<~EO_HELP;
    NAME:                     $prog

    DESCRIPTION:
        Serve trasparent images to be used as overlays for brouter-web.
        The images show the borders of map tiles in zoom level 14, i.e.
        the border of the tiles relevant for ride-every-tile.

    SYNOPSIS: $prog [-h14 ] [ -p PORT ]

    OPTIONS:
        --help | -h | -?
                Print this little help and exit.
        --ipv4-only | -4
                Just use IPv4 as IPv6 might be slow on ancient systems.
        --port PORT | -p PORT
                Listen on port PORT.  Defaults to $default_server_port.
        --min-zoom Z | --mz Z | -m Z
                Minimum zoon leve to produce overlay for. Defaults to 11.
    EO_HELP
    exit 0;
}
